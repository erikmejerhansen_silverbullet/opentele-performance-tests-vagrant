Vagrant box creation for OpenTele Server performance tests
===============
Project for creating [Vagrant](http://www.vagrantup.com/) boxes for use in OpenTele Server performance tests.

How to build and run
--------------------
Make sure you have Vagrant installed.
Run `vagrant up`
After `vagrant up` finishes you can run
`vagrant ssh -c 'tail -f /srv/tomcat/opentele-server/logs/catalina.out'`
and
`vagrant ssh -c 'tail -f /srv/tomcat/opentele-citizen-server/logs/catalina.out'`
to follow the progress of the deployment of OpenTele.
Once OpenTele has deployed, opentele-server can be found at `http://localhost:4567`
and opentele-citizen-server can be found at `http://localhost:4568`.

You can then run integration tests with
`mvn gatling:execute -Dgatling.simulationClass=[Test-class to run] -Dserver=[host:port/context] -Dusers=[number of users] -Dramp=[ramp time]`
After running the tests run `vagrant destroy` to reset the test system.
>>>>>>> 6bea5b9... Version 2.0.3

After vagrant has finished running export a new vagrant box by running
`vagrant package --base OpenTele_performance_test_box --output opentele-performance-test.box`

The new box is now ready to be used in performance tests.

Collecting CPU, Memory and Disk usage
-------------------------------------
There are scripts included for collecting CPU, Memory and Disk usage.
You can use the following flow to collect data
```
vagrant ssh -c 'nohup /vagrant/scripts/start.sh'
mvn gatling:execute -Dgatling.simulationClass=[Test-class to run] -Dserver=[host:port/context] -Dusers=[number of users] -Dramp=[ramp time]
vagrant ssh -c '/vagrant/scripts/stop.sh'
vagrant ssh -c '/vagrant/scripts/report.sh'
``
The last step generates graphs and these can then be found in the `reports` folder.
The raw data can be found in the `stats` folder.
>>>>>>> 6bea5b9... Version 2.0.3

Current setup
-----------
The current setup creates an Ubuntu based machine with the following products
* Apache Tomcat 7
* MySQL
* Oracle JDK 7
